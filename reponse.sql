-- 1. Liste des potions : Numéro, libellé, formule et constituant principal. (5 lignes)
SELECT * FROM potion

-- 2. Liste des noms des trophées rapportant 3 points. (2 lignes)
SELECT NomCateg FROM categorie WHERE NbPoints = 3

-- 3. Liste des villages (noms) contenant plus de 35 huttes. (4 lignes)
SELECT NomVillage FROM village WHERE NbHuttes > 35

-- 4. Liste des trophées (numéros) pris en mai / juin 52. (4 lignes)
SELECT NumTrophee FROM trophee WHERE DatePrise BETWEEN "2052-05-01" AND "2052-06-30"

-- 5. Noms des habitants commençant par 'a' et contenant la lettre 'r'. (3 lignes)
SELECT Nom FROM habitant WHERE Nom LIKE "a%r%"

-- 6. Numéros des habitants ayant bu les potions numéros 1, 3 ou 4. (8 lignes)
SELECT DISTINCT NumHab FROM absorber WHERE NumPotion != 2

-- 7. Liste des trophées : numéro, date de prise, nom de la catégorie et nom du preneur. (10lignes)
SELECT NumTrophee,DatePrise,NomCateg, Nom FROM trophee INNER JOIN habitant ON trophee.NumPreneur = habitant.NumHab INNER JOIN categorie ON trophee.CodeCat = categorie.CodeCat;

-- 8. Nom des habitants qui habitent à Aquilona. (7 lignes)
SELECT nom FROM habitant INNER JOIN village ON habitant.NumVillage = village.NumVillage WHERE NomVillage LIKE "Aquilona";

-- 9. Nom des habitants ayant pris des trophées de catégorie Bouclier de Légat. (2 lignes)
SELECT nom FROM habitant INNER JOIN trophee ON trophee.NumPreneur = habitant.NumHab  INNER JOIN categorie ON trophee.CodeCat = categorie.CodeCat WHERE NomCateg LIKE "Bouclier de Légat";

-- 10. Liste des potions (libellés) fabriquées par Panoramix : libellé, formule et constituant principal. (3 lignes)
SELECT DISTINCT LibPotion FROM potion INNER JOIN fabriquer ON potion.NumPotion = fabriquer.NumPotion INNER JOIN habitant ON fabriquer.NumHab = habitant.NumHab WHERE Nom LIKE "Panoramix";

-- 11. Liste des potions (libellés) absorbées par Homéopatix. (2 lignes)
SELECT DISTINCT LibPotion FROM potion INNER JOIN absorber ON potion.NumPotion = absorber.NumPotion INNER JOIN habitant ON absorber.NumHab = habitant.NumHab WHERE Nom LIKE "Homéopatix";

-- 12. Liste des habitants (noms) ayant absorbé une potion fabriquée par l'habitant numéro.3 (4 lignes) 
SELECT DISTINCT nom FROM habitant INNER JOIN absorber ON habitant.NumHab = absorber.NumHab INNER JOIN fabriquer ON absorber.NumPotion = fabriquer.NumPotion WHERE fabriquer.NumHab = 3;

-- 13. Liste des habitants (noms) ayant absorbé une potion fabriquée par Amnésix. (7 lignes)
SELECT DISTINCT nom FROM habitant INNER JOIN absorber ON habitant.NumHab = absorber.NumHab INNER JOIN fabriquer ON absorber.NumPotion = fabriquer.NumPotion WHERE fabriquer.NumHab = (SELECT NumHab FROM habitant WHERE nom LIKE "Amnésix");

-- 14. Nom des habitants dont la qualité n'est pas renseignée. (3 lignes)
SELECT nom FROM habitant WHERE NumQualite IS NULL;

-- 15. Nom des habitants ayant consommé la potion magique n°1 (c'est le libellé de la potion) en février 52. (3 lignes)
SELECT nom FROM habitant INNER JOIN absorber ON habitant.NumHab = absorber.NumHab INNER JOIN potion ON absorber.NumPotion = potion.NumPotion WHERE potion.NumPotion = (SELECT NumPotion FROM potion WHERE LibPotion LIKE "Potion magique n°1") AND absorber.DateA BETWEEN "2052-02-01" AND "2052-02-28";

-- 16. Nom et âge des habitants par ordre alphabétique. (22 lignes)
SELECT nom,age FROM habitant ORDER BY nom;

-- 17. Liste des resserres classées de la plus grande à la plus petite : nom de resserre et nom du village. (3 lignes)
SELECT NomResserre, Nomvillage FROM resserre INNER JOIN village ON resserre.NumVillage = village.NumVillage ORDER BY Superficie;
-- ***
-- 18. Nombre d'habitants du village numéro 5. (4)
SELECT COUNT(*) FROM habitant WHERE habitant.NumVillage = 5;

-- 19. Nombre de points gagnés par Goudurix. (5)
SELECT SUM(NbPoints) FROM categorie INNER JOIN trophee ON categorie.CodeCat = trophee.CodeCat INNER JOIN habitant ON trophee.NumPreneur = habitant.NumHab WHERE habitant.Nom LIKE "Goudurix";
 
-- 20. Date de première prise de trophée. (03/04/52)
SELECT DatePrise FROM trophee LIMIT 1;

-- 21. Nombre de louches de potion magique n°2 (c'est le libellé de la potion) absorbées. (19)
SELECT SUM(Quantite) FROM absorber INNER JOIN potion ON absorber.NumPotion = potion.NumPotion WHERE potion.LibPotion LIKE "Potion magique n°2";

-- 22. Superficie la plus grande. (895)
SELECT Superficie FROM resserre ORDER BY Superficie DESC LIMIT 1;

-- 23. Nombre d'habitants par village (nom du village, nombre). (7 lignes)
SELECT NomVillage, COUNT(*) FROM habitant INNER JOIN village ON habitant.NumVillage = village.NumVillage GROUP BY village.NumVillage;

-- 24. Nombre de trophées par habitant (6 lignes)
SELECT NumPreneur, COUNT(*) FROM trophee INNER JOIN habitant ON trophee.NumPreneur = habitant.NumHab GROUP BY habitant.NumHab;

-- 25. Moyenne d'âge des habitants par province (nom de province, calcul). (3 lignes)
SELECT NomProvince , AVG(habitant.Age) FROM province INNER JOIN village ON province.NumProvince = village.NumProvince INNER JOIN habitant ON village.NumVillage = habitant.NumVillage GROUP BY NomProvince;

-- 26. Nombre de potions différentes absorbées par chaque habitant (nom et nombre). (9 lignes)
SELECT Nom, COUNT(DISTINCT potion.NumPotion) FROM potion INNER JOIN absorber ON potion.NumPotion = absorber.NumPotion INNER JOIN habitant ON absorber.NumHab = habitant.NumHab GROUP BY Nom; 

-- 27. Nom des habitants ayant bu plus de 2 louches de potion zen. (1 ligne)
SELECT Nom FROM habitant INNER JOIN absorber ON habitant.NumHab = absorber.NumHab INNER JOIN potion ON absorber.NumPotion = potion.NumPotion WHERE LibPotion LIKE "Potion Zen" AND Quantite > 2;

-- 28. Noms des villages dans lesquels on trouve une resserre (3 lignes)
SELECT NomVillage FROM village INNER JOIN resserre ON village.NumVillage = resserre.NumVillage;

-- 29. Nom du village contenant le plus grand nombre de huttes. (Gergovie)
SELECT NomVillage FROM village ORDER BY NbHuttes DESC LIMIT 1;

-- 30. Noms des habitants ayant pris plus de trophées qu'Obélix (3 lignes)
SELECT Nom FROM (SELECT Nom,COUNT(*) AS nbt FROM trophee INNER JOIN habitant ON trophee.NumPreneur = habitant.NumHab GROUP BY Nom) AS HabT WHERE HabT.nbt > (SELECT COUNT(*) FROM trophee INNER JOIN habitant ON trophee.NumPreneur = habitant.NumHab WHERE Nom LIKE "Obélix");

